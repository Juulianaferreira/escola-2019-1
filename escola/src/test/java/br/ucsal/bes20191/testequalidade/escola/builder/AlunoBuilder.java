package br.ucsal.bes20191.testequalidade.escola.builder;

import br.ucsal.bes20191.testequalidade.escola.domain.*;

public class AlunoBuilder {
	
	private static final Integer MATRICULA_DEFAULT =1;
	private static final String NOME_DEFAULT = "Juliana";
	private static final SituacaoAluno SITUACAO_DEFAULT = null;
	private static final Integer ANO_NASCIMENTO_DEFAULT = 2003;
	
	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private SituacaoAluno situacao = SITUACAO_DEFAULT;
	private Integer anoNascimento = ANO_NASCIMENTO_DEFAULT;
	
	private AlunoBuilder(){
		
	}
	
	public static AlunoBuilder umAluno(){
		return new AlunoBuilder();
	}
	
	public static AlunoBuilder umAlunoCancelado(){
		return new AlunoBuilder().cancelado();
	}

	public AlunoBuilder comMatricula(Integer matricula){
		this.matricula = matricula;
		return this;
	}
	
	public AlunoBuilder comNome(String nome){
		this.nome = nome;
		return this;
	}
	
	public AlunoBuilder semMatricula(){
		this.matricula = null;
		return this;
	}
	
	public AlunoBuilder cancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}
	
	public AlunoBuilder ativo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}
	
	public AlunoBuilder comDataNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}
	
	public AlunoBuilder mas(){
		AlunoBuilder alunoBuilder = new AlunoBuilder();
		alunoBuilder.nome = nome; 
		alunoBuilder.situacao = situacao; 
		alunoBuilder.matricula = matricula; 
		alunoBuilder.anoNascimento = anoNascimento; 
		return alunoBuilder; 
	}
	
	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setNome(nome);
		aluno.setSituacao(situacao);
		aluno.setMatricula(matricula);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

}
