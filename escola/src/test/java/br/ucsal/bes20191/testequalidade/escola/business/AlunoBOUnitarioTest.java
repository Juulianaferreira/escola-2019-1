package br.ucsal.bes20191.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.testng.Assert;

import br.ucsal.bes20191.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Comanda.class })

public class AlunoBOUnitarioTest {

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
	 * 16 anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(1).comDataNascimento(2003).build();	
		AlunoDAO alunoDAO = new AlunoDAO();
		alunoDAO.encontrarPorMatricula(1);
		
		AlunoBO aluno1 = new AlunoBO().calcularIdade(1);
		AlunoBO spy = PowerMockito.spy(aluno1);

		PowerMockito.mockStatic(AlunoBO.class);
		spy.calcularIdade(1);
		
		Integer resultadoEsperado = 16;
		Integer resultadoAtual = WhiteBox.invokeMehod(spy, "calcularIdade");
		
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
	}
	
	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	public void testarAtualizacaoAlunosAtivos() {
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(1).ativo().build();
		AlunoDAO alunoDAO = new AlunoDAO();
		alunoDAO.encontrarPorMatricula(1);
		
		AlunoBO aluno1 = new AlunoBO().atualizar(aluno1);
		AlunoBO spy = PowerMockito.spy(aluno1);


		PowerMockito.mockStatic(AlunoBO.class);
		spy.atualizar(aluno1);
		
		PowerMockito.verifyStatic();
		PowerMockito.verifyNoMoreInteractions(AlunoBO.class);
	}


}
