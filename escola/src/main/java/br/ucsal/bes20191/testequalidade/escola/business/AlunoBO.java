package br.ucsal.bes20191.testequalidade.escola.business;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;

public class AlunoBO {

	private AlunoDAO alunoDAO;

	private DateHelper dateUtil;

	public AlunoBO(AlunoDAO alunoDAO, DateHelper dateUtil) {
		this.alunoDAO = alunoDAO;
		this.dateUtil = dateUtil;
	}

	public void atualizar(Aluno aluno) {
		if (SituacaoAluno.ATIVO.equals(aluno.getSituacao())) {
			alunoDAO.salvar(aluno);
		}
	}

	public Integer calcularIdade(Integer matricula) {
		Aluno aluno = alunoDAO.encontrarPorMatricula(matricula);
		return dateUtil.obterAnoAtual() - aluno.getAnoNascimento();
	}

}
